package com.example.kvizapp

data class Question( val textResId: Int, val answer: Boolean)