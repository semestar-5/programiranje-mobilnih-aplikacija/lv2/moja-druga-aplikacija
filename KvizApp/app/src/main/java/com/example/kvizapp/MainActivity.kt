package com.example.kvizapp

import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.util.Log

private const val TAG = "MainActivity"
class MainActivity : AppCompatActivity() {

    private lateinit var btnTrue : Button
    private lateinit var btnFalse : Button
    private lateinit var btnNext: Button
    private lateinit var btnPrevious: Button
    private lateinit var txtView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnTrue=findViewById(R.id.btnTrue)
        btnFalse=findViewById(R.id.btnFalse)
        btnNext=findViewById(R.id.btnNext)
        btnPrevious=findViewById(R.id.btnPrevious)
        txtView=findViewById(R.id.txtView)

        var currentIndex: Int =0

         val questionList = listOf(
            Question(R.string.pitanje1,true),
            Question(R.string.pitanje2,false),
             Question(R.string.pitanje3,false),
             Question(R.string.pitanje4,true),
             Question(R.string.pitanje5,false),
             Question(R.string.pitanje6,true),
             Question(R.string.pitanje7,false),
             Question(R.string.pitanje8,true),
             Question(R.string.pitanje9,true),
             Question(R.string.pitanje10,true),
             Question(R.string.pitanje11,true)


        )

        txtView.setText(questionList[currentIndex].textResId)

        fun checkAnswer(userAnswer: Boolean){
            val correctAnswer = questionList[currentIndex].answer
            val messageResId = if (userAnswer == correctAnswer) {
                R.string.toast_true
            } else {
                R.string.toast_false
            }
            Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
                .show()
        }

        fun nextQuestion(){
            currentIndex = (currentIndex + 1) % 11
            txtView.setText(questionList[currentIndex].textResId)
        }

        fun previousQuestion(){
            currentIndex = (currentIndex - 1) % 11
            txtView.setText(questionList[currentIndex].textResId)
        }

        btnTrue.setOnClickListener{
            checkAnswer(true)
        }

        btnFalse.setOnClickListener{
            checkAnswer(false)
        }

        btnNext.setOnClickListener{
            nextQuestion()
        }
        btnPrevious.setOnClickListener{
            if(currentIndex!=0){
                previousQuestion()
            }

        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }
    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")}


    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }
    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }
}